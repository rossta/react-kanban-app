import React from 'react';
import Note from './note';

export default class Notes extends React.Component {
  constructor(props: {
    items: Array;
    onEdit: Function;
  }) {
    super(props);
  }

  render() {
    var notes = this.props.items;

    return (
        <ul className='notes'>
          {notes.map((note, i) =>
            <li className='note' key={'note' + i}>
              <Note
                value={note.task}
                onEdit={this.props.onEdit.bind(null, i)} />
              <button onClick={() => this.removeItem(i)}>-</button>
            </li>
          )}
        </ul>
    );
  }

  removeItem(i) {
    this.props.onEdit(i, null);
  }

}
